using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts;

public class ScoreDisplayScript : MonoBehaviour
{
    public int level = 1;

    // Start is called before the first frame update
    void Start()
    {
        float score = -1;
        if(ScoreController.Level1Seconds != -1 && level == 1)
        {
            score = ScoreController.Level1Seconds;
        }
        if (ScoreController.Level2Seconds != -1 && level == 2)
        {
            score = ScoreController.Level2Seconds;
        }
        if (ScoreController.Level3Seconds != -1 && level == 3)
        {
            score = ScoreController.Level3Seconds;
        }
        if (ScoreController.Level4Seconds != -1 && level == 4)
        {
            score = ScoreController.Level4Seconds;
        }
        if (ScoreController.Level5Seconds != -1 && level == 5)
        {
            score = ScoreController.Level5Seconds;
        }
        if (ScoreController.Level6Seconds != -1 && level == 6)
        {
            score = ScoreController.Level6Seconds;
        }
        if (score != -1)
        {
            GetComponent<Text>().text += score + "s";
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
