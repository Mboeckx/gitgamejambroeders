using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts;

public class SheepScript : MonoBehaviour
{
    //bool particlesplaying = false;
    GameObject sheepPortal;
    float timeToPlay = 2f;
    public float SecondsBetweenRandomTurns = 2f;
    float lastDirectionChangeElapsed = 0f;
    Vector3 direction;
    public float SheepSpeed = 2f;
    int randomNumber;
    // Start is called before the first frame update
    void Start()
    {
        lastDirectionChangeElapsed = 0f;
        //particlesplaying = false;
        direction = Vector3.left;
        //gameObject.GetComponent<ParticleSystem>().Stop();
    }

    // Update is called once per frame
    void Update()
    {
        if (ScoreController.StartedLevel) { 
        //GetComponent<Rigidbody>().AddForce(direction * SheepSpeed);
        lastDirectionChangeElapsed += Time.deltaTime;
            if (lastDirectionChangeElapsed > SecondsBetweenRandomTurns)
            {
                randomNumber = Random.Range(0, 9);
                GetComponent<Rigidbody>().AddForce(-(direction * SheepSpeed));

                switch (randomNumber)
                {
                    case 0:
                        direction = new Vector3(0, 0, -1);
                        break;
                    case 1:
                        direction = new Vector3(-1, 0, 0);
                        break;
                    case 2:
                        direction = new Vector3(1, 0, 0);
                        break;
                    case 3:
                        direction = new Vector3(0, 0, 1);
                        break;
                    case 4:
                        direction = new Vector3(0, 1, 0);
                        break;
                    default:
                        break;
                }
                GetComponent<Rigidbody>().AddForce(direction * SheepSpeed);

                lastDirectionChangeElapsed = 0f;
            }
        }

        //if (particlesplaying)
        //{
        //    timeToPlay -= Time.deltaTime;
        //    if(timeToPlay <= 0)
        //    {
        //        sheepPortal.GetComponent<ParticleSystem>().Stop();
        //    }
        //}
    }


        private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "SheepPortal")
        {
            ScoreController.CollectSheep();
            //sheepPortal = collision.collider.gameObject;
            //bool particlesplaying = true;
            //sheepPortal.GetComponent<ParticleSystem>().Play();
            Destroy(gameObject);
        }
    }
}
