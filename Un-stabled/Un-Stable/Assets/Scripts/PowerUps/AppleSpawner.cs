using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppleSpawner : MonoBehaviour
{
    public GameObject Apple;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        SpawnApple();
    }

    void SpawnApple()
    {
        var appleObj = Instantiate(Apple, transform.position + new Vector3(0f, 5f, 0f), new Quaternion(), transform);
        appleObj.GetComponent<Rigidbody>().AddExplosionForce(1000f, new Vector3(0f, 5f, 0f), 4f, 5.0F);
    }
}
