using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChickenScript : MonoBehaviour
{
    public Canvas KipPopup;
    public float selfDestructInSeconds = -1f;
    public bool SpinChicken = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public Vector3 Velocity;

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Vector3 pos = new Vector3(-2500f, 0, 0);
            Vector3 popupPos = new Vector3(transform.position.x, transform.position.y + 5f, transform.position.z);
            KipPopup.transform.position = popupPos;
            Instantiate(KipPopup);

            collision.collider.gameObject.GetComponent<Rigidbody>().AddForce(pos);
        }
    }

    public void Update()
    {
        transform.position += Velocity * Time.deltaTime;

        if (SpinChicken)
        {
            var rotation = transform.rotation;
            rotation.y += 1f * Time.deltaTime;
            transform.rotation = rotation;
        }

        if(selfDestructInSeconds > 0)
        {
            Destroy(gameObject, selfDestructInSeconds);
        }
    }
}
