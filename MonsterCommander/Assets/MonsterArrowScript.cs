using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterArrowScript : MonoBehaviour
{
    public GameObject LeftArrow;
    public GameObject RightArrow;
    public GameObject UpArrow;
    public GameObject DownArrow;

    SpriteRenderer leftSpriteRenderer;
    SpriteRenderer rightSpriteRenderer;
    SpriteRenderer upSpriteRenderer;
    SpriteRenderer downSpriteRenderer;


    // Start is called before the first frame update
    void Start()
    {
        leftSpriteRenderer = LeftArrow.GetComponent<SpriteRenderer>();
        rightSpriteRenderer = RightArrow.GetComponent<SpriteRenderer>();
        upSpriteRenderer = UpArrow.GetComponent<SpriteRenderer>();
        downSpriteRenderer = DownArrow.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetArrow(Vector3 direction)
    {
        LeftArrow.SetActive(false);
        RightArrow.SetActive(false);
        UpArrow.SetActive(false);
        DownArrow.SetActive(false);

        if(direction == Vector3.left)
        {
            LeftArrow.SetActive(true);
        }else if(direction == Vector3.right)
        {
            RightArrow.SetActive(true);
        }else if (direction == Vector3.up)
        {
            UpArrow.SetActive(true);
        }
        else if (direction == Vector3.down)
        {
            DownArrow.SetActive(true);
        }
    }
}
