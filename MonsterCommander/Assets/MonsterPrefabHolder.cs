using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterPrefabHolder : MonoBehaviour
{
    public GameObject BlueMonsterPrefab;
    public GameObject RedMonsterPrefab;
    public GameObject GreenMonsterPrefab;
    public GameObject YellowMonsterPrefab;
}
