using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManaMeterScript : MonoBehaviour
{
    public PlayerColor player;
    public float MaxX;
    public float MinX;
    private float step;

    [HideInInspector]
    public PlayerScript PlayerScript;

    public TMPro.TMP_Text manaText;

    // Start is called before the first frame update
    void Start()
    {
        step = (MaxX - MinX) / GameManager.MaxManaLimit;
    }

    public void ActivatePlayerScoreBoard()
    {
        GetComponentInParent<ScoreWizardColorSetter>().Activate();
    }
    public void SetScoreBoardToStandby()
    {
        GetComponentInParent<ScoreWizardColorSetter>().SetStandby();
    }

    public void VisualizeMana(int mana)
    {
        transform.localPosition = new Vector2(MinX + (step * mana), transform.localPosition.y);
        manaText.text = mana.ToString();
        if(GameManager.MaxManaLimit / mana < (GameManager.MaxManaLimit * 0.15f) || GameManager.MaxManaLimit /mana > GameManager.MaxManaLimit * 0.85f)
        {
            manaText.color = Color.red;
        }
        else
        {
            manaText.color = Color.black;
        }
    }

}
