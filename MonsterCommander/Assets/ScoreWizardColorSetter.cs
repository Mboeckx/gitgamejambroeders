using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreWizardColorSetter : MonoBehaviour
{
    public Color color;
    private SpriteRenderer spriteRenderer;
    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        SetTransparencyOfSpriteRenderer(0f);
    }

    private void SetTransparencyOfSpriteRenderer(float aValue)
    {
        Color tempcolor = spriteRenderer.color;
        tempcolor.a = aValue;
        spriteRenderer.color = tempcolor;
    }

    internal void SetStandby()
    {
        SetTransparencyOfSpriteRenderer(255f);
        spriteRenderer.material.SetColor("_Color", Color.grey);
    }

    internal void Activate()
    {
        spriteRenderer.material.SetColor("_Color", color);
    }
}
