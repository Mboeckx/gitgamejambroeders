using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    private bool startbuttonwasreleased = false;
    private int first60framesCounter = 0;

    private ManaMeterScript manaMeter;
    private bool isReady = false;

    public int PlayerNumber;
    public PlayerColor PlayerColor_Name;
    public Color PlayerColor_Color;
    public int Score { get; set; }
    public int PowerForNextWaterElemental { get; set; }
    public int PowerForNextFireElemental { get; set; }
    public int PowerForNextForestElemental { get; set; }
    public int PowerForNextThunderElemental { get; set; }

    public int Mana = 50; //You need mana to survive, too much or too little and you die

    public int ManaProduction = 1; //killing enemy monsters gets you mana

    // Start is called before the first frame update
    void Start()
    {
        isReady = false;
        first60framesCounter = 0;
        var thisPlayer = GameManager.PossiblePlayers[GameManager.amountPlayers];
        PlayerColor_Name = thisPlayer.PlayerColor;
        PlayerColor_Color = thisPlayer.PlayerColor_Color;
        GetComponent<MovementScript>().PlayerScript = this;
        GetComponent<MovementScript>().SetInitialPosition(thisPlayer.StartingCoordinates);
        PlayerNumber = GameManager.amountPlayers;
        GameManager.amountPlayers++;

        GetComponent<MovementScript>().movePoint.GetComponent<SpriteRenderer>().material.SetColor("_Color", PlayerColor_Color);
        GetComponent<SpriteRenderer>().material.SetColor("_Color", PlayerColor_Color);

        PowerForNextFireElemental = GameManager.DefaultMonsterPower;
        PowerForNextForestElemental = GameManager.DefaultMonsterPower;
        PowerForNextThunderElemental = GameManager.DefaultMonsterPower;
        PowerForNextWaterElemental = GameManager.DefaultMonsterPower;
        SetStartingMana();

        manaMeter = FindObjectsOfType<ManaMeterScript>(true).First(x => x.player == PlayerColor_Name);
        manaMeter.SetScoreBoardToStandby();
        


    }

    private void SetStartingMana()
    {
        //When jumping into the game midway through, get the average mana of all players
        var allPlayerScripts = FindObjectsOfType<PlayerScript>();
        if (allPlayerScripts.Length > 1)
        {
            Mana = Mathf.RoundToInt((float)allPlayerScripts.Where(x => x != this).Average(x => x.Mana));
        }
    }

    public void AddMana(int manaAmount)
    {
        Mana += manaAmount;
        manaMeter.VisualizeMana(Mana);
    }

    public void AddMana(ManaAction manaAction)
    {
        Mana += ManaValuesForActions.ManaAwardedForAction[manaAction];
        manaMeter.VisualizeMana(Mana);
    }

    public void ConfirmReady()
    {
        if (!isReady)
        {
            isReady = true;
            GameManager.playersReady++;
            manaMeter.ActivatePlayerScoreBoard();
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(first60framesCounter < 60)
        {
            first60framesCounter++;
        }
        else
        {
            startbuttonwasreleased = true;
        }
    }

    void OnReadyButton()
    {
        if(first60framesCounter >= 60)
        {
            ConfirmReady();
        }

    }
}
