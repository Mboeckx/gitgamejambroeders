using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class MovementScript : MonoBehaviour
{
    //voor teleport, zet walkspeed op 50 of hoger voor teleport
    private int tellersindslaatstecursormove = 0;
    private int cursorMoveMogelijkPerXFrames;

    public Transform movePoint;
    private Vector3 currentMovepointPosition;
    float walkspeed;
    Vector2 movement;
    public Animator animator;
    bool hasnotplayedthisturn;
    bool isSpawningMonster;

    Vector3 confirmedMovepointPosition;


    Vector3 DirectionToAdd;

    public PlayerScript PlayerScript;


    public LayerMask WhatStopsMovement;

    // Start is called before the first frame update
    void Start()
    {
        cursorMoveMogelijkPerXFrames = 5;
        if (GameManager.IsTeleportAllowed)
        {
            walkspeed = 50f;
        }
        else
        {
            walkspeed = 5f;
        }


            movement = new Vector2();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.GameInProgress)
        {
            if (GameManager.MovementSignal > 0)
            {
                GetComponent<MonsterSpawnScript>().MoveAllMonsters();
                if (hasnotplayedthisturn)
                {
                    hasnotplayedthisturn = false;
                    GameManager.MovementSignal--;
                    if (!isSpawningMonster)
                    {
                        //move
                        if (transform.position == movePoint.position)
                        {
                            //Standing still generates more mana
                            PlayerScript.AddMana(ManaAction.StandStill);
                        }
                        else
                        {
                            //walking generates mana
                            PlayerScript.AddMana(ManaAction.Move);
                            confirmedMovepointPosition = movePoint.position;
                            currentMovepointPosition = movePoint.position;
                        }
                        

                    }
                    else
                    {
                        //Spawn monster
                        if(DirectionToAdd != Vector3.zero)
                        {
                            GetComponent<MonsterSpawnScript>().SpawnMonster(movePoint.position, DirectionToAdd, PlayerScript.PlayerNumber, PlayerScript.PlayerColor_Color, walkspeed, PlayerScript);
                            DirectionToAdd = Vector3.zero;

                            isSpawningMonster = false;
                        }
                        else
                        {
                            isSpawningMonster = false;
                            PlayerScript.AddMana(ManaAction.StandStill);
                        }
                        movePoint.position = transform.position;
                        currentMovepointPosition = movePoint.position;
                    }
                }
            }
            else
            {
                hasnotplayedthisturn = true;
            }

            

            SetMovementAnimations();
        }
    }

    private void FixedUpdate()
    {
        if (GameManager.GameInProgress)
        {
            //Teleport logica
            if (GameManager.IsTeleportAllowed)
            {
                tellersindslaatstecursormove++;
                if (tellersindslaatstecursormove >= cursorMoveMogelijkPerXFrames)
                {
                    tellersindslaatstecursormove = 0;
                    //Laat na x frames terug toe dat de cursor beweegt vanaf die locatie
                    currentMovepointPosition = movePoint.position;
                }
            }


            UpdateMovement();

            SetMovePoint();
        }
    }

    private void UpdateMovement()
    {
        if (GameManager.IsHakkelMovement)
        {
            transform.localPosition = Vector3.MoveTowards(transform.position, confirmedMovepointPosition,10f); //10 om in hakkels te bewegen, normaal walkspeed * deltatime
        }
        else
        {
            transform.localPosition = Vector3.MoveTowards(transform.position, confirmedMovepointPosition, walkspeed * Time.deltaTime); //1 om in hakkels te bewegen, normaal walkspeed * deltatime
        }
    }

    internal void SetInitialPosition(Vector2 coordinates)
    {
        transform.position = coordinates;
        currentMovepointPosition = movePoint.position;
        movePoint.parent = null;
        confirmedMovepointPosition = movePoint.position;
    }

    private void SetMovePoint()
    {
        //if(Vector3.Distance(transform.position, movePoint.position) == 0f)
        //{
        if (Mathf.Abs(movement.x) > Mathf.Abs(movement.y))
        {
            if (movement.x > 0.01f)
            {
                if (!Physics2D.OverlapCircle(currentMovepointPosition + new Vector3(1f, 0f), 0.4f, WhatStopsMovement))
                {
                    //movePoint.position += new Vector3(1f, 0f);
                    movePoint.position = currentMovepointPosition + new Vector3(1f, 0f);
                    DirectionToAdd = new Vector3(1f, 0f);
                }


            }
            else if (movement.x < -0.01f)
            {
                if (!Physics2D.OverlapCircle(currentMovepointPosition + new Vector3(-1f, 0f), 0.4f, WhatStopsMovement))
                {
                    movePoint.position = currentMovepointPosition + new Vector3(-1f, 0f);
                    DirectionToAdd = new Vector3(-1f, 0f);
                }
            }
        }
        else
        {
            if (movement.y > 0.01f)
            {
                if (!Physics2D.OverlapCircle(currentMovepointPosition + new Vector3(0f, 1f), 0.4f, WhatStopsMovement))
                {
                    movePoint.position = currentMovepointPosition + new Vector3(0f, 1f);
                    DirectionToAdd = new Vector3(0f, 1f);

                }
                    
            }
            else if (movement.y < -0.01f)
            {
                if (!Physics2D.OverlapCircle(currentMovepointPosition + new Vector3(0f, -1f), 0.4f, WhatStopsMovement))
                {
                    movePoint.position = currentMovepointPosition + new Vector3(0f, -1f);
                    DirectionToAdd = new Vector3(0f, -1f);

                }
                    
            }
            else
            {
                if(movePoint.position == transform.position)
                {
                    DirectionToAdd = new Vector3();
                }
            }
        }
        //}
    }

    private void SetMovementAnimations()
    {
        animator.SetFloat("Horizontal", movement.x);
        animator.SetFloat("Vertical", movement.y);
        animator.SetFloat("Speed", movement.sqrMagnitude);
    }

    void OnMove(InputValue movementValue)
    {
        if (GameManager.GameInProgress)
        {
            Vector2 movementVector = movementValue.Get<Vector2>();
            movement.x = movementVector.x;
            movement.y = movementVector.y;
        }
    }

    void OnSpawnRed(InputValue inputValue)
    {
        SetIsSpawningMonster();
    }

    private void SetIsSpawningMonster()
    {
        if (GameManager.GameInProgress)
            isSpawningMonster = true;
    }

    void OnSpawnBlue(InputValue inputValue)
    {
        SetIsSpawningMonster();
    }

    void OnSpawnYellow(InputValue inputValue)
    {
        SetIsSpawningMonster();
    }

    void OnSpawnGreen(InputValue inputValue)
    {
        SetIsSpawningMonster();
    }
}
