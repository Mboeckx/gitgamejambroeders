using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicalObjectType : MonoBehaviour
{
    public MagicalObjectTypeEnum ThisType;

    public MagicalObjectTypeEnum GetWeakness()
    {
        if(ThisType == MagicalObjectTypeEnum.Fire)
        {
            return MagicalObjectTypeEnum.Water;
        }
        else if (ThisType == MagicalObjectTypeEnum.Water)
        {
            return MagicalObjectTypeEnum.Forest;
        }
        else if (ThisType == MagicalObjectTypeEnum.Forest)
        {
            return MagicalObjectTypeEnum.Fire;
        }
        else
        {
            return MagicalObjectTypeEnum.Bullet;
        }
    }
}


public enum MagicalObjectTypeEnum
{
    Wizard, Fire, Water, Forest, Bullet
}