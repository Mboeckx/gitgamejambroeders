using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class MonsterSpawnScript : MonoBehaviour
{
    public List<MonsterScript> monstersSpawned;

    private PlayerScript playerScript;

    MonsterColor ColorToSpawn;
    GameObject PrefabToSpawn;

    // Start is called before the first frame update
    void Start()
    {
        playerScript = GetComponent<PlayerScript>();
        monstersSpawned = new List<MonsterScript>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    internal void SpawnMonster(Vector3 position, Vector3 direction, int playerNumber, Color playerColor, float walkSpeed, PlayerScript playerScript)
    {
        var monster = Instantiate(PrefabToSpawn, position,new Quaternion(), null);
        var monscript = monster.GetComponent<MonsterScript>();
        monscript.playerScript = playerScript;
        monscript.PlayerNumber = playerNumber;
        monscript.PlayerColor = playerColor;
        //monscript.WalkSpeed = walkSpeed;
        monscript.SetDirection(direction);
        monscript.RenderColor();

        monstersSpawned.Add(monscript);
    }

    void OnSpawnRed(InputValue inputValue)
    {
        if (GameManager.GameInProgress)
        {
            PrefabToSpawn = FindObjectOfType<MonsterPrefabHolder>().RedMonsterPrefab;
            ColorToSpawn = MonsterColor.Red;
        }
    }

    void OnSpawnBlue(InputValue inputValue)
    {
        if (GameManager.GameInProgress)
        {
            PrefabToSpawn = FindObjectOfType<MonsterPrefabHolder>().BlueMonsterPrefab;
            ColorToSpawn = MonsterColor.Blue;
        }
    }

    void OnSpawnYellow(InputValue inputValue)
    {
        if (GameManager.GameInProgress)
        {
            PrefabToSpawn = FindObjectOfType<MonsterPrefabHolder>().YellowMonsterPrefab;
            ColorToSpawn = MonsterColor.Yellow;
        }
    }

    void OnSpawnGreen(InputValue inputValue)
    {
        if (GameManager.GameInProgress)
        {
            PrefabToSpawn = FindObjectOfType<MonsterPrefabHolder>().GreenMonsterPrefab;
            ColorToSpawn = MonsterColor.Green;
        }
    }

    internal void MoveAllMonsters()
    {
        foreach(var monster in monstersSpawned)
        {
            if(monster != null)
            {
                playerScript.AddMana(ManaAction.MonsterUpkeep);
                monster.Move();
            }
        }
    }
}
