using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static bool IsTeleportAllowed = true;
    public static bool IsHakkelMovement = false;

    public Image GameInactivePanel;
    private Color InactivePanelInitialColor;

    public static bool GameInProgress;
    public static int MovementSignal;
    private int framespassed;
    public static int amountPlayers;
    public static int playersReady;

    public static List<PossiblePlayer> PossiblePlayers = new List<PossiblePlayer>() { new PossiblePlayer(Color.blue, PlayerColor.Blue, new Vector2(-8f, 7f)), new PossiblePlayer(Color.red, PlayerColor.Red, new Vector2(13f, 7f)), new PossiblePlayer(Color.yellow, PlayerColor.Yellow, new Vector2(-8f, -7f)), new PossiblePlayer(Color.green, PlayerColor.Green, new Vector2(13f, -7f)), new PossiblePlayer(Color.white, PlayerColor.White, new Vector2(0f, 0f)) };

    public static int MaxManaLimit = 100;
    public static int DefaultManaDrainPerTurn = 2;

    public static int DefaultMonsterPower = 2;

    public int secondsPerMove = 2;
    private int MoveTimerInFrames;
    public Image TimerImage;

    // Start is called before the first frame update
    void Start()
    {
        MoveTimerInFrames = secondsPerMove * 60;
        InactivePanelInitialColor = GameInactivePanel.color;
        GameInProgress = false;
        amountPlayers = 0;
        framespassed =0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if (GameInProgress)
        {
            framespassed++;
            TimerImage.fillAmount += 1f / MoveTimerInFrames;
            if (framespassed == MoveTimerInFrames)
            {
                MovementSignal = amountPlayers;
                framespassed = 0;
                TimerImage.fillAmount = 0;
            }
        }
        else
        {
            if(playersReady == amountPlayers && playersReady != 0)
            {
                GameInactivePanel.color = new Color(0, 0, 0, 0);
                GameInProgress = true;
            }
            else
            {
                GameInactivePanel.color = InactivePanelInitialColor;
            }
        }
    }
}

public class PossiblePlayer
{
    public Color PlayerColor_Color { get; set; }
    public PlayerColor PlayerColor { get; set; }
    public Vector2 StartingCoordinates { get; set; }

    public PossiblePlayer(Color color, PlayerColor playerColor, Vector2 startCoordinates)
    {
        PlayerColor = playerColor;
        PlayerColor_Color = color;
        StartingCoordinates = startCoordinates;
    }
}

public enum MonsterColor
{
    Red, Blue, Yellow, Green
}

public enum PlayerColor
{
    Red, Blue, Yellow, Green, White
}

public enum ManaAction
{
    StandStill, Move, SpawnMonster, MonsterUpkeep, YourMonsterKilledAMonster, YourMonsterAttackedAHuman, YourMonsterDied, WizardAttackHitEnemyWizard, WizardGotHitByEnemyWizardAttack, 
}

public static class ManaValuesForActions
{
    public static Dictionary<ManaAction, int> ManaAwardedForAction = new Dictionary<ManaAction, int>()
    {
        {ManaAction.StandStill, 1 },
        {ManaAction.Move, 0 },
        {ManaAction.SpawnMonster, 0 },
        {ManaAction.MonsterUpkeep, 1 },
        {ManaAction.YourMonsterKilledAMonster, 5 },
        {ManaAction.YourMonsterAttackedAHuman, 7 },
        {ManaAction.YourMonsterDied, -10 },
        {ManaAction.WizardAttackHitEnemyWizard, 5 },
        {ManaAction.WizardGotHitByEnemyWizardAttack, -10 }
    };
}