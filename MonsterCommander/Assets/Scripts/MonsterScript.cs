using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MonsterScript : MonoBehaviour
{
    public MonsterArrowScript MonsterArrowScript;

    private MagicalObjectType ThisMagicalObjectType;

    public LayerMask WhatStopsMovement;
    public LayerMask DoesDamageTo;

    public SpriteRenderer ThisSpriteRenderer;

    [HideInInspector]
    public float WalkSpeed;

    public SpriteRenderer PlayerRing;
    private Vector3 Direction { get; set; }
    public int PlayerNumber { get; set; }
    public Color PlayerColor { get; set; }
    public MonsterColor MonsterColor { get; set; }

    public PlayerScript playerScript;

    private Vector3 confirmedMovepointPosition;

    // Start is called before the first frame update
    void Start()
    {
        WalkSpeed = 5f;
        confirmedMovepointPosition = ThisSpriteRenderer.transform.position;
        ThisMagicalObjectType = ThisSpriteRenderer.GetComponent<MagicalObjectType>();
    }

    public void SetDirection(Vector3 direction)
    {
        Direction = direction;
        MonsterArrowScript.SetArrow(direction);
        FlipSpriteIfNecessary();
    }

    internal void Move()
    {
        if (!Physics2D.OverlapCircle(ThisSpriteRenderer.transform.position + Direction, 0.2f, WhatStopsMovement))
        {
            FlipSpriteIfNecessary();

            confirmedMovepointPosition = ThisSpriteRenderer.transform.position + Direction;

        }
        else
        {
            //omdraaien of aanvallen
            ThisSpriteRenderer.flipX = !ThisSpriteRenderer.flipX;
            SetDirection(-Direction);
        }
    }

    public void Kill(bool hostile = true)
    {
        if (hostile)
        {
            playerScript.AddMana(ManaAction.YourMonsterDied);
        }
        
        Destroy(transform.gameObject);
        Destroy(this);
    }

    private void FlipSpriteIfNecessary()
    {
        if (!ThisSpriteRenderer.flipX && (Direction == Vector3.left || Direction == Vector3.up))
        {
            ThisSpriteRenderer.flipX = true;
        }
        else if (ThisSpriteRenderer.flipX && !(Direction == Vector3.left || Direction == Vector3.up))
        {
            ThisSpriteRenderer.flipX = false;
        }
    }

    private void Update()
    {
        CheckCollision();
    }
    private void FixedUpdate()
    {
        UpdateMovement();
    }

    private void CheckCollision()
    {
        var colliders = Physics2D.OverlapCircleAll(ThisSpriteRenderer.transform.position, 0.2f, DoesDamageTo);
        if(colliders.Length > 1)
        {
            //Get the first object that isn't this one.
            var collider = colliders.First(x => x.gameObject != ThisSpriteRenderer.gameObject);

            if (collider)
            {
                var othermon = collider.transform.parent.GetComponent<MonsterScript>();
                if (othermon.playerScript.PlayerColor_Name != playerScript.PlayerColor_Name)
                {
                    var enemyMagicalObjectType = collider.gameObject.GetComponent<MagicalObjectType>();

                    if (enemyMagicalObjectType.GetWeakness() == ThisMagicalObjectType.ThisType)
                    {
                        //Destroy target creature
                        othermon.Kill();
                        //TODO:Effect sprite or something and a sound

                    }
                    else if (enemyMagicalObjectType.ThisType == ThisMagicalObjectType.ThisType)
                    {
                        //Destroy both
                        othermon.Kill();
                        Kill();
                    }
                }
            }
        }
        
    }

    private void UpdateMovement()
    {
        if (GameManager.IsHakkelMovement)
        {
            ThisSpriteRenderer.transform.position = Vector3.MoveTowards(ThisSpriteRenderer.transform.position, confirmedMovepointPosition, 1f); //1 om in hakkels te bewegen, normaal walkspeed * deltatime
        }
        else
        {
            ThisSpriteRenderer.transform.position = Vector3.MoveTowards(ThisSpriteRenderer.transform.position, confirmedMovepointPosition, WalkSpeed * Time.deltaTime); //1 om in hakkels te bewegen, normaal walkspeed * deltatime
        }
    }

    internal void RenderColor()
    {
        PlayerRing.color = PlayerColor;
    }
}
