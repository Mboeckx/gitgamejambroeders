using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalSettings
{
    public static int currentHoofd = 0;
    public static int currentBody = 0;
    public static int currentHand = 0;

    public static ControlScheme controlScheme = ControlScheme.VirtualStickTouch;
    public static float gyroYOffset = 0f;

    public static int HighScore { get; internal set; }
    public static bool GyroHeldUp { get; internal set; } = false;

    public static float BGMVolume = 1f;
    public static float SFXVolume = 1f;

}
