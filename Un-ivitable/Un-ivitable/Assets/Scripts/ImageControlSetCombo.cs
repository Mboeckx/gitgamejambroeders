﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    [Serializable]
    public class ImageControlSetCombo
    {
        public ImageControlSetCombo()
        {

        }
        [SerializeField]
        public Sprite Image;
        [SerializeField]
        public ControlScheme ControlSet;
    }
}
