using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpawnPointHolder : MonoBehaviour
{
    public List<Transform> SpawnPointCollection;
    // Start is called before the first frame update
    void Start()
    {
        SpawnPointCollection = new List<Transform>();
        SpawnPointCollection = GetComponentsInChildren<Transform>().ToList();

        SpawnPointCollection.Remove(transform);
    }
}
