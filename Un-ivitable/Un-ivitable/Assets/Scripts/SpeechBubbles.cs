using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SpeechBubbles
{
    public static List<string> StraffeUitspraken = new List<string>()
    {
        "Oh boy, it's hot today!",
        "I hate mondays",
        "Hello, i'm Dave",
        "Why am I here?",
        "I want coffee",
        "Coffee is my lifeblood",
        "Don't talk to me before I had my coffee",
        "I bless the rains down in Africa",
        "Ik ruik een plantje denk ik",
        "Have you seen [popular movie] last night?",
        "Jawadde",
        "I like this office",
        "I think I just saw a horse",
        "My life is very interesting",
        "Wanna hear me talk about myself?",
        "COFFEEEEE",
        "I think that guy stopped breathing",
        "Does anyone know first aid?",
        "Look at this funny video",
        "My moves are amazing",
        "Slackbot is my best friend",
        "Ever think about jumping out of one of those windows?",
"I dreamt that I was a plant yesterday...",
"I'd like to touch a plant.",
"Ever played Un-Stabled? Great game!",
"This is a cry for help, I am stuck in this office.",
"My favourite colour is green",
"We work here all day and night!",
"Was that Shrek?",
"Sometimes I just look outside all day...",
"Don't tell the manager I''m just standing here, ok?",
"I'm not sure how I even got here.",
"I forgot what my job is actually.",
"Ich kann perfectes Deutsch sprechen ja.",
"Someone should clean the coffee machine",
"I think the copy machine is broken.",
"I want different hands",
"Don't look at me like that",
"I feel like we're being watched",
"I love this fresh office breeze",
"Something smells weird here",
"Where can I find the bathroom",
"Could you get me the report on the average lifespans of lobsters by next monday?",
"GEE I WONDER IF SAYING SOMETHING REALLY LONG WILL BE ANNOYING FOR SOMEBODY OBSERVING US AND/OR TRYING TO SAVE THAT ONE LOVELY PLANT FROM DYING, BUT I SUPPOSE IT WON'T REALLY"
    };

    public static List<string> OorlogspadUitspraken = new List<string>()
    {
        "DEATH TO ALL PLANTS!",
        "Did you know I'm vegan?",
        "Can I touch that?",
        "OH BOY I LOVE TOUCHING PLANTS",
        "Touch plant? Touch plant.",
        "What a nice plant you have there. Shame if something would... happen.",
        "Please, let me hold it!",
        "IS THAT A PLANT?!",
        "Give me that!",
        "Don't mind me",
        "I just wanna touch it a little bit",
        "Nobody's gonna know"
    };

    public static List<string> IkBenDoodMeldingen = new List<string>()
    {
        "I'm having a little trouble breathing",
        "AWCH!",
        "WHYYYYY?!",
        "*dies*",
        "Only... a setback...",
        "'Tis just a flesh wound!",
        "Heart... pounding... vision... fading",
        "I just wanted to touch the little plant",
        "That plant deserved my touch!",
        "I thought we were friends",
        "The floor is so cold",
        "I feel so cold",
        "Tell my wife I loved this plant",
        "Flub",
        "That hurt!",
        "Stop punching me!",
        "I've fallen and I can't get up"
    };




    public static List<string> GameOverUitspraken = new List<string>()
    {
        "Oh no!",
        "Whelp",
        "Nobody saw that coming.",
        "You'll do better next time!",
        "What have you done?!",
        "This is all your fault, poor plant!",
        "Can I have the remains?",
        "Oh... I wonder if it's edible.",
        "I'm so sorry",
        "That plant deserved to live",
        "Our most prized posession!!!",
        "Het is kapot",
        "I expected more from you",
        "I had such high expectations from you",
        "Your manager will hear about this!"
    };

    public static List<string> GameOverInsults = new List<string>()
    {
        "You tried",
        "RIP F",
        "Game over",
        "Mission Failed",
        "Fission Mailed",
        "PLANT? PLANT?! PLAAAAAANT!!!!",
        "That plant will never see tomorrow",
        "You failed",
        "So sad",
        "You could've done way better",
        "That didn't work out",
        "This was inevitable",
        "Try harder",
        "You're fired",
        "Report to management",
        "I just knew you'd fail"
    };
}
