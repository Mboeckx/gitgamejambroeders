using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable] //om te kunnen editen in de inspector
public class BodyStyle
{
    public Material Materials;
    public Mesh Meshes;
    public string Gender;
    public string Tag;
}

public class BodyStyleHolder : MonoBehaviour
{

    //Singleton
    public static BodyStyleHolder Instance;

    public BodyStyle[] BodyStyles;

    void Awake()
    {
        Instance = this;
    }
}