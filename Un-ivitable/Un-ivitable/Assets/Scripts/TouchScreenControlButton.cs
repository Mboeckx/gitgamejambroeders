﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts
{
    class TouchScreenControlButton : Button
    {
        public override void OnPointerDown(PointerEventData eventData)
        {
            base.OnPointerDown(eventData);
            //Debug.Log("Down");
            if(transform.parent.name == "WaterOnScreenButton")
            {
                GameManager.WaterButtonPressed = true;
            }
            else if(transform.parent.name == "FillOnScreenButton")
            {
                GameManager.FillButtonPressed = true;
            }
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            base.OnPointerUp(eventData);
            //Debug.Log("Up");
            if (transform.parent.name == "WaterOnScreenButton")
            {
                GameManager.WaterButtonPressed = false;
            }
            else if (transform.parent.name == "FillOnScreenButton")
            {
                GameManager.FillButtonPressed = false;
            }
        }
    }


}
