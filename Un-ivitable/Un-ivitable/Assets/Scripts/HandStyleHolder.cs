using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable] //om te kunnen editen in de inspector
public class HandStyle
{
    public Material Materials;
    public Mesh Meshes;
    public string Tag;
}

public class HandStyleHolder : MonoBehaviour
{

    //Singleton
    public static HandStyleHolder Instance;

    public HandStyle[] HandStyles;

    void Awake()
    {
        Instance = this;
    }
}