using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SpeechBubbleBehavior : MonoBehaviour
{
    float TimeLeftForCurrentState;
    public AIBehavior LinkedVentje;

    public Transform LinkedHead;

    private AudioEffectsHolder audioEffectsHolder;
    private List<AudioEffects> praatAudioEffects;
    private List<AudioEffects> pijnAudioEffects;

    public float HoeVeelMagIkPratenOpTien = 3f;

    Text textField;
    Image bubble;

    Color bubbleColor;
    Color textColor;

    // Start is called before the first frame update
    void Start()
    {
        bubble = GetComponentInChildren<Image>();
        textField = GetComponentInChildren<Text>();
        bubbleColor = bubble.color;
        textColor = textField.color;

        //zwijg initieel
        TimeLeftForCurrentState = 2f;
        bubbleColor.a = 0f;
        textColor.a = 0f;
        transform.Rotate(0, 90f, 0f);

        textField.color = textColor;
        bubble.color = bubbleColor;

        LinkedHead = LinkedVentje.transform.Find("Hoofd");
        audioEffectsHolder = GameObject.Find("HolderDeBolder").GetComponent<AudioEffectsHolder>();
        praatAudioEffects = audioEffectsHolder.AudioEffects.Where(x => x.Tag == "praat").ToList();
        pijnAudioEffects = audioEffectsHolder.AudioEffects.Where(x => x.Tag == "pijn").ToList();

        DecideNextText();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(LinkedHead.position.x, LinkedHead.position.y + 2f, LinkedHead.position.z);

        if (TimeLeftForCurrentState <= 0)
        {
            DecideNextText();
        }
    }

    private void FixedUpdate()
    {
        TimeLeftForCurrentState -= (1f / 60f);
    }

    public void DecideNextText(bool forceTalk = false)
    {
        TimeLeftForCurrentState = Random.Range(2f, 5f);

        //niet altijd praten
        if((Random.Range(0f,10f) < HoeVeelMagIkPratenOpTien) || forceTalk)
        {
            //zeg spul
            if(LinkedVentje != null)
            {
                if (GameManager.gameover)
                {
                    textField.text = SpeechBubbles.GameOverUitspraken[Random.Range(0, SpeechBubbles.GameOverUitspraken.Count)];
                }
                else
                {
                    if (LinkedVentje.Health <= 0)
                    {
                        //dood
                        textField.text = SpeechBubbles.IkBenDoodMeldingen[Random.Range(0, SpeechBubbles.IkBenDoodMeldingen.Count)];
                        audioEffectsHolder.PlayAudioClip(pijnAudioEffects[Random.Range(0, pijnAudioEffects.Count)].clip);
                    }
                    else if (LinkedVentje.TijdTotGoestingOmBloemAanTeRaken <= 0f)
                    {
                        //Goesting om bloem aan te raken
                        textField.text = SpeechBubbles.OorlogspadUitspraken[Random.Range(0, SpeechBubbles.OorlogspadUitspraken.Count)];
                        audioEffectsHolder.PlayAudioClip(praatAudioEffects[Random.Range(0, praatAudioEffects.Count)].clip);
                    }
                    else
                    {
                        //chit chat
                        textField.text = SpeechBubbles.StraffeUitspraken[Random.Range(0, SpeechBubbles.StraffeUitspraken.Count)];
                        audioEffectsHolder.PlayAudioClip(praatAudioEffects[Random.Range(0,praatAudioEffects.Count)].clip);
                    }
                }
                

                bubbleColor.a = 200f;
                textColor.a = 200f;

                textField.color = textColor;
                bubble.color = bubbleColor;
            }
        }else
        {
            //zwijg
            bubbleColor.a = 0f;
            textColor.a = 0f;

            textField.color = textColor;
            bubble.color = bubbleColor;
        }
    }
}
