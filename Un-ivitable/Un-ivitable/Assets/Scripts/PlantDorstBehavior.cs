using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlantDorstBehavior : MonoBehaviour
{
    public bloemPotKleinBehavior bloempotBehavior;

    public Mesh NewMesh;
    public Material NewPalette;

    public Material BrownPalette;

    public Mesh OldMesh;
    public Material OldPalette;

    public float DorstMeter;
    public float MaxDorstMeter = 100f;
    public bool IsPlantAlive;

    private float krijgDorst = 1f / 60f;
    private float DorstMultiplier = 1f;

    private Slider PlantHUD;
    private Text PotGoneAlert;

    // Start is called before the first frame update
    void Start()
    {
        DorstMultiplier = 1f;
        DorstMeter = MaxDorstMeter;
        IsPlantAlive = true;

        OldMesh = GetComponent<MeshFilter>().sharedMesh;
        OldPalette = GetComponent<MeshRenderer>().sharedMaterial;

        PlantHUD = GameObject.Find("PlantHUD").GetComponent<Slider>();
        PotGoneAlert = GameObject.Find("PlantHUD/PotGoneAlert").GetComponent<Text>();
        PotGoneAlert.enabled = false;

        if (PlantHUD != null)
        {
            PlantHUD.maxValue = MaxDorstMeter;
            PlantHUD.image.color = Color.green;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(PlantHUD != null)
        {
            PlantHUD.value = DorstMeter;
        }
        
        if (IsPlantAlive)
        {
            if (DorstMeter <= 0)
            {
                MakePlantDead();
            }
            else if (DorstMeter <= 10)
            {
                MakePlantBrown();
            }
            else if (DorstMeter < MaxDorstMeter / 2)
            {
                MakePlantSad();
            }
            else if (DorstMeter >= MaxDorstMeter / 2)
            {
                MakePlantHappy();
            }
        }
    }

    void FixedUpdate()
    {
        if (IsPlantAlive)
        {
            DorstMeter -= krijgDorst * DorstMultiplier;
        }
    }

    void MakePlantBrown()
    {
        PlantHUD.image.color = Color.red;
        GetComponent<MeshRenderer>().sharedMaterial = BrownPalette;
        transform.parent.Find("BloemPot").GetComponentInChildren<PulsateLightScript>().SetColor(Color.red);

    }

    void MakePlantSad()
    {
        PlantHUD.image.color = Color.yellow;
        GetComponent<MeshFilter>().sharedMesh = NewMesh;
        GetComponent<MeshRenderer>().sharedMaterial = NewPalette;
        //GetComponent<MeshCollider>().sharedMesh = NewMesh;
        transform.parent.Find("BloemPot").GetComponentInChildren<PulsateLightScript>().SetColor(Color.yellow);
    }

    void MakePlantHappy()
    {
        PlantHUD.image.color = Color.green;
        GetComponent<MeshFilter>().sharedMesh = OldMesh;
        GetComponent<MeshRenderer>().sharedMaterial = OldPalette;
        //GetComponent<MeshCollider>().sharedMesh = OldMesh;
        transform.parent.Find("BloemPot").GetComponentInChildren<PulsateLightScript>().SetColor(Color.green);
    }

    public void MakePlantDead()
    {
        if (IsPlantAlive)
        {
            IsPlantAlive = false;
            GameManager.gameover = true;

            MakePlantBrown();
            bloempotBehavior.Explode();
        }
    }

    public void ActivatePanikMode()
    {
        if (IsPlantAlive)
        {
            PotGoneAlert.enabled = true;
            DorstMultiplier = 1.3f;
        }
    }

    public void GivePlantWater(float amount)
    {
        DorstMeter += amount;
        if (DorstMeter > MaxDorstMeter)
        {
            DorstMeter = MaxDorstMeter;
        }
    }
}
