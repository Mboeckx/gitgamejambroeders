using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebrisUpdateScript : MonoBehaviour
{
    public Vector3 Velocity;
    public float selfDestructInSeconds = 10f;
    public bool spinRandom = false;

    // Start is called before the first frame update
    void Start()
    {    
    }

 
    public void Update()
    {
        transform.position += Velocity * Time.deltaTime;
        if (selfDestructInSeconds > 0)
        {
            Destroy(gameObject, selfDestructInSeconds);
        }

        if (spinRandom)
        {
            var rotation = transform.rotation;
            rotation.y += 1f * Time.deltaTime;
            transform.rotation = rotation;
        }
    }
}
