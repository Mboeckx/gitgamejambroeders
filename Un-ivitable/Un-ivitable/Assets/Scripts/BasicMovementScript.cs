using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicMovementScript : MonoBehaviour
{
    public LevelBounds levelBounds;
    public GameObject virtualStickObject;
    private FloatingJoystick virtualStick;


    private int speed = 10;

    private float speedModifier = 1f;
    private float gyroYOffset = 0f;

    Vector3 CurrentMovement;

    // Start is called before the first frame update
    void Start()
    {
        virtualStickObject.SetActive(false);

        levelBounds = new LevelBounds();
        //Level 1 bounds
        levelBounds.LeftBorderX = -28.1f;
        levelBounds.RightBorderX = 28.1f;
        levelBounds.BackBorderZ = 15f;
        levelBounds.FrontBorderZ = -15.2f;

        if(GlobalSettings.controlScheme == Assets.Scripts.ControlScheme.WASD)
        {
            speedModifier = 1f;
            GameObject.Find("Overlay/TouchButtons").SetActive(false);
        }
        else if(GlobalSettings.controlScheme == Assets.Scripts.ControlScheme.GyroTouch)
        {
            //if (GlobalSettings.GyroHeldUp)
            //{
            //    gyroYOffset = 0.75f;
            //}
            gyroYOffset = GlobalSettings.gyroYOffset;
            GameObject.Find("Overlay/controls").SetActive(false);
            speedModifier = 3f;
        }else if(GlobalSettings.controlScheme == Assets.Scripts.ControlScheme.VirtualStickTouch)
        {
            virtualStickObject.SetActive(true);
            virtualStick = virtualStickObject.GetComponentInChildren<FloatingJoystick>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (GlobalSettings.controlScheme == Assets.Scripts.ControlScheme.WASD)
        {
            CurrentMovement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        }
        else if (GlobalSettings.controlScheme == Assets.Scripts.ControlScheme.GyroTouch)
        {
            CurrentMovement = new Vector3(Input.acceleration.x, 0, Input.acceleration.y + gyroYOffset);
        }
        else if (GlobalSettings.controlScheme == Assets.Scripts.ControlScheme.VirtualStickTouch)
        {
            CurrentMovement = new Vector3(virtualStick.Horizontal, 0, virtualStick.Vertical);
        }
    }

    private void FixedUpdate()
    {
        Vector3 newPos = transform.position + CurrentMovement * speed * speedModifier * Time.deltaTime;
        if (newPos.x > levelBounds.LeftBorderX && newPos.x < levelBounds.RightBorderX && newPos.z > levelBounds.FrontBorderZ && newPos.z < levelBounds.BackBorderZ)
        {
            transform.position = newPos;
        }
        
        //transform.GetComponent<Rigidbody>().AddForce(CurrentMovement * speed * speedModifier * Time.deltaTime);
    }
}


public class LevelBounds
{
    public LevelBounds()
    {

    }
    public float LeftBorderX;
    public float RightBorderX;
    public float BackBorderZ;
    public float FrontBorderZ;
}
