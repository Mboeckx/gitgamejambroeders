using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable] //om te kunnen editen in de inspector
public class HeadStyle
{
    public Material Materials;
    public Mesh Meshes;
    public string Gender;
    public string Tag;
}

public class HeadStyleHolder : MonoBehaviour
{

    //Singleton
    public static HeadStyleHolder Instance;

    public HeadStyle[] HeadStyles;

    void Awake()
    {
        Instance = this;
    }
}