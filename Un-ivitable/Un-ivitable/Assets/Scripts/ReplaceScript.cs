using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReplaceScript : MonoBehaviour
{
    public Mesh NewMesh;
    public Material NewPalette;

    private Mesh OldMesh;
    private Material OldPalette;

    private bool isNewState;

    // Start is called before the first frame update
    void Start()
    {
        OldMesh = GetComponent<MeshFilter>().sharedMesh;
        OldPalette = GetComponent<MeshRenderer>().sharedMaterial;

        isNewState = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            if (!isNewState)
            {
                GetComponent<MeshFilter>().sharedMesh = NewMesh;
                GetComponent<MeshRenderer>().sharedMaterial = NewPalette;
                GetComponent<MeshCollider>().sharedMesh = NewMesh;
                isNewState = true;
            }
            else
            {
                GetComponent<MeshFilter>().sharedMesh = OldMesh;
                GetComponent<MeshRenderer>().sharedMaterial = OldPalette;
                GetComponent<MeshCollider>().sharedMesh = OldMesh;
                isNewState = false;
            }
            
        }
    }
}
