using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToolTipImageChanger : MonoBehaviour
{
    [SerializeField]
    public List<ImageControlSetCombo> controlSetCombos; 

    // Start is called before the first frame update
    void Start()
    {
        transform.Find("Image").GetComponent<Image>().sprite = controlSetCombos[(int)GlobalSettings.controlScheme].Image;
    }
}
