using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class VolleGieterbehavior : MonoBehaviour
{
    public GameObject GietEffect;
    public GameObject LeegEffect;
    public GameObject VulEffect;
    public float HoeveelheidWater;
    public float MaximumWater = 100f;
    public float GieterRadius = 10f;

    private GameObject gietEffectInstance;
    private GameObject leegEffectInstance;
    private GameObject vulEffectInstance;
    private bool isGieten;
    private bool isVullen;

    private Collider plantColliderInRange;
    private GameObject plantLastInRange;

    private Collider waterbronColliderInRange;
    private GameObject waterbronLastInRange;

    private float geefWater = 1f / 10f;

    private GameObject GieterHUDObject;
    private Slider GieterHUD;

    private GameObject TouchButtons;
    private Button waterButton;
    private Button fillButton;
    private Image waterDisableOverlay;
    private Image fillDisableOverlay;

    public bool IsOpgeraapt;
    // Start is called before the first frame update
    void Start()
    {
        TouchButtons = GameObject.Find("Overlay/TouchButtons");

        waterButton = TouchButtons.transform.Find("WaterOnScreenButton").GetComponentInChildren<Button>();
        fillButton = TouchButtons.transform.Find("FillOnScreenButton").GetComponentInChildren<Button>();
        waterDisableOverlay = TouchButtons.transform.Find("WaterOnScreenButton").Find("DisableButton").GetComponent<Image>();
        fillDisableOverlay = TouchButtons.transform.Find("FillOnScreenButton").Find("DisableButton").GetComponent<Image>();
        SetButtonState(fillButton, false, fillDisableOverlay);
        SetButtonState(waterButton, false, waterDisableOverlay);

        // unless specified different
        HoeveelheidWater = MaximumWater;
        isGieten = false;
        IsOpgeraapt = false;
        GieterHUDObject = GameObject.Find("GieterHUD");
        GieterHUD = GieterHUDObject.GetComponent<Slider>();
        if(GieterHUD != null)
        {
            GieterHUD.maxValue = MaximumWater;
            
            GieterHUDObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (GieterHUD != null)
        {
            GieterHUD.value = HoeveelheidWater;
        }
        
        if (IsOpgeraapt)
        {
            if ((Input.GetKey(KeyCode.Mouse0) && GlobalSettings.controlScheme == Assets.Scripts.ControlScheme.WASD) || GameManager.WaterButtonPressed)
            {
                isVullen = false;
                if (HoeveelheidWater <= 0)
                {
                    StopGieten();
                    GieterIsLeeg();
                }
                else
                {
                    StartGieten();
                }
            }
            else if ((Input.GetKey(KeyCode.Mouse1) && GlobalSettings.controlScheme == Assets.Scripts.ControlScheme.WASD)|| GameManager.FillButtonPressed)
            {
                //vullen
                isVullen = true;
            }
            else
            {
                isVullen = false;
                StopGieten();
                StopVullen();
                if (leegEffectInstance != null)
                {
                    Destroy(leegEffectInstance);
                }
            }

            // --- houdt de gieter vast --
            var player = GameObject.FindGameObjectWithTag("Player");
            if (player != null)
            {
                HouGieterVast(player);
            }
        }
    }

    public void HouGieterVast(GameObject player)
    {
        //plaats in handen
        var ventje = player;

        GetComponentInChildren<PulsateLightScript>().Stop = true;

        if (player.tag != "Player")
        {
            if (player.transform.parent.tag == "Player")
            {
                ventje = player.transform.parent.gameObject;
            }
        }
        if (ventje != null)
        {
            var hand = ventje.transform.Find("RHand");
            if (hand == null)
            {
                hand = ventje.transform.parent.Find("RHand");
            }

            var handposition = hand.transform.position; ;
            var handrotation = hand.transform.rotation;

            if (hand != null)
            {
                transform.position = new Vector3(handposition.x, handposition.y, handposition.z);
                transform.rotation = new Quaternion(handrotation.x, handrotation.y, handrotation.z, handrotation.w);

                GieterHUDObject.SetActive(true);

                IsOpgeraapt = true;

                var mesh = GetComponent<MeshCollider>();
                if (mesh != null)
                {
                    Destroy(mesh);
                }
            }
        }

        var waterbronnen = GameObject.FindGameObjectsWithTag("Waterbron");
        foreach(var bron in waterbronnen)
        {
            var light = bron.GetComponentInChildren<PulsateLightScript>();
            if(light != null)
            {
                light.Stop = false;
            }
           
        }

    }

    void VulGieter(float hoeveelheid)
    {
        if (HoeveelheidWater < MaximumWater)
        {
            HoeveelheidWater += hoeveelheid;
            if (HoeveelheidWater > MaximumWater)
            {
                HoeveelheidWater = MaximumWater;
                StopVullen();
            }

            if (vulEffectInstance == null)
            {
                vulEffectInstance = Instantiate(VulEffect);
            }
        }
    }

    void StopVullen()
    {
        if (vulEffectInstance != null)
        {
            Destroy(vulEffectInstance);
        }
    }

    // called 60 times per second
    private void FixedUpdate()
    {
        var colliders = Physics.OverlapSphere(transform.position, GieterRadius);

        int aantalWaterkwijt = 1;
        foreach (var collider in colliders)
        {
            if (isGieten)
            {
                if (collider.gameObject.tag == "Plant")
                {
                    PlantDorstBehavior plant = collider.gameObject.GetComponent<PlantDorstBehavior>();
                    if (plant != null)
                    {
                        // nekel de PLANT heeft dit script, en dus dorst
                        aantalWaterkwijt++;
                        plant.GivePlantWater(geefWater);
                    }
                }
            }
            else if (isVullen)
            {
                if (collider.gameObject.tag == "Waterbron")
                {
                    // vul dubbel zo snel als we m leeggieten
                    VulGieter(geefWater * 2);
                }
            }


        }

        CheckForTooltips(colliders);

        if (isGieten)
        {
            HoeveelheidWater -= (geefWater * aantalWaterkwijt);
        }
    }

    private void CheckForTooltips(Collider[] colliders)
    {
        plantColliderInRange = colliders.FirstOrDefault(x => x.gameObject.tag == "Plant");
        if (plantColliderInRange != null && IsOpgeraapt)
        {
            plantColliderInRange.gameObject.transform.GetComponent<TooltipActivationScript>().Tooltip.enabled = true;
            SetButtonState(waterButton, true, waterDisableOverlay);
            plantLastInRange = plantColliderInRange.gameObject;
        }
        else
        {
            if (plantLastInRange != null)
            {
                plantLastInRange.transform.GetComponent<TooltipActivationScript>().Tooltip.enabled = false;
                SetButtonState(waterButton, false, waterDisableOverlay);
                plantLastInRange = null;
            }
        }

        waterbronColliderInRange = colliders.FirstOrDefault(x => x.gameObject.tag == "Waterbron");
        if (waterbronColliderInRange != null)
        {
            waterbronColliderInRange.gameObject.transform.GetComponent<TooltipActivationScript>().Tooltip.enabled = true;
            SetButtonState(fillButton, true, fillDisableOverlay);
            waterbronLastInRange = waterbronColliderInRange.gameObject;
        }
        else
        {
            if (waterbronLastInRange != null)
            {
                waterbronLastInRange.transform.GetComponent<TooltipActivationScript>().Tooltip.enabled = false;
                SetButtonState(fillButton, false, fillDisableOverlay);
                waterbronLastInRange = null;
            }
        }
    }

    void SetButtonState(Button button, bool state, Image extraDisableLayer = null)
    {
        if(button != null)
        {
            button.interactable = state;
            if(extraDisableLayer != null)
            {
                extraDisableLayer.enabled = !state;
            }
        }
    }

    void StartGieten()
    {
        isGieten = true;
        var spuit = transform.parent.Find("Waterspuit");
        if (gietEffectInstance == null)
        {
            //doe de gieter gieten
            if (spuit != null)
            {
                var rot = spuit.rotation;
                var rotation = Quaternion.FromToRotation(Vector3.up, new Vector3(rot.x, rot.x, rot.x));
                var pos = spuit.position;
                Vector3 position = new Vector3(pos.x, pos.y, pos.z);
                gietEffectInstance = Instantiate(GietEffect, position, rotation);
                gietEffectInstance.GetComponentInChildren<AudioSource>().volume = GlobalSettings.SFXVolume;
            }
        }
        else
        {
            // update de waterpositie en rotatie
            if (spuit != null)
            {
                var rot = spuit.rotation;
                var rotation = Quaternion.FromToRotation(Vector3.up, new Vector3(rot.x, rot.x, rot.x));
                var pos = spuit.position;
                Vector3 position = new Vector3(pos.x, pos.y, pos.z);
                gietEffectInstance.transform.position = position;
                gietEffectInstance.transform.rotation = rotation;
            }
        }
    }

    void StopGieten()
    {
        isGieten = false;
        if (gietEffectInstance != null)
        {
            Destroy(gietEffectInstance);
        }
    }

    void GieterIsLeeg()
    {
        if (HoeveelheidWater <= 0)
        {
            if (leegEffectInstance == null)
            {
                leegEffectInstance = Instantiate(LeegEffect);
            }
        }
    }
}
