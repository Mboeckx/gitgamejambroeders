using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebrisScript : MonoBehaviour
{
    public GameObject[] debris;
    public float MinSecondsBetweenRandoms = 3f;
    public float MaxSecondsBetweenRandoms = 3f;
    public float MinRandomSpeedMod = 10f;
    public float MaxRandomSpeedMod = 10f;
    public float sizemodmin = 1f;
    public float sizemodmax = 2f;
    public float RandomLifetime = 3f;
    public Vector3 Velocity;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnRandom", Random.Range(MinSecondsBetweenRandoms, MaxSecondsBetweenRandoms), Random.Range(MinSecondsBetweenRandoms, MaxSecondsBetweenRandoms));
    }

    void SpawnRandom()
    {
        int arraylength = debris.Length;
        Vector3 VelocityInstance = new Vector3();
        VelocityInstance.x = Velocity.x;
        VelocityInstance.y = Velocity.y;
        VelocityInstance.z = Velocity.z;

        VelocityInstance.x = Velocity.x + Random.Range(MinRandomSpeedMod, MaxRandomSpeedMod);


        var newRandom = GameObject.Instantiate(debris[Random.Range(0, arraylength)]);
        //newRandom.transform.SetPositionAndRotation(new Vector3(transform.position.x, transform.position.y + 2f, transform.position.z), newRandom.transform.rotation);
        var sizemod = Random.Range(sizemodmin, sizemodmax);
        newRandom.transform.localScale = new Vector3(sizemod, sizemod, sizemod);
        //newRandom.GetComponent<Rigidbody>().velocity += new Vector3(Random.Range(-20,20), Random.Range(-20, 20), Random.Range(-20, 20));
        newRandom.AddComponent<DebrisUpdateScript>().selfDestructInSeconds = RandomLifetime;
        newRandom.AddComponent<DebrisUpdateScript>().Velocity = VelocityInstance;
        var rb = newRandom.AddComponent<Rigidbody>();
        rb.isKinematic = true;
        rb.mass = 9999f;

        newRandom.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        newRandom.transform.Rotate(new Vector3(0, -90, 0));
        switch (Random.Range(0, 100))
        {
            case 10:
                newRandom.transform.Rotate(new Vector3(0, 90, 0));
                break;
            case 20:
                newRandom.transform.Rotate(new Vector3(0, 0, 0));
                break;
            case 30:
                newRandom.transform.Rotate(new Vector3(90, -90, 0));
                break;
            case 40:
                newRandom.transform.Rotate(new Vector3(90, -90, -90));
                break;
            case 50:
                newRandom.AddComponent<DebrisUpdateScript>().spinRandom = true;
                break;
            case 51:
                newRandom.AddComponent<DebrisUpdateScript>().spinRandom = true;
                break;
        }



    }

    public void Update()
    {
        //transform.position += Velocity * Time.deltaTime;

       
    }
}

