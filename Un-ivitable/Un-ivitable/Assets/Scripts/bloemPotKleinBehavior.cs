using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bloemPotKleinBehavior : MonoBehaviour
{
    public PlantDorstBehavior plantBehavior;

    public GameObject Explosion;

    private GameObject explosionInstance;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (explosionInstance != null)
        {
            Destroy(explosionInstance, 2);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if(this.tag == "Plant")
        {
            foreach (ContactPoint contact in collision.contacts)
            {
                if (contact.point.y <= 3)
                {
                    plantBehavior.MakePlantDead();
                }
            }
        }else if(this.tag == "Pot")
        {
            foreach (ContactPoint contact in collision.contacts)
            {
                if (contact.point.y <= 3)
                {
                    //pot is weg, meer dorst
                    plantBehavior.ActivatePanikMode();
                }
            }
        }
        
    }

    public void Explode()
    {
        var rotation = Quaternion.FromToRotation(Vector3.up, new Vector3(0, 0, 0));
        var transform = GetComponent<Transform>().position;
        Vector3 position = new Vector3(transform.x + 2, transform.y + 3, transform.z);
        explosionInstance = Instantiate(Explosion, position, rotation);
    }
}
