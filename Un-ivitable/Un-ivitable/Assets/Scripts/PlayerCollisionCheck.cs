using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisionCheck : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Untagged":
                // kunne we niks mee doen
                break;
            case "Player":
            ///FIGHT FIGHT FIGHT
            ///break;
            case "Collega":
                //als ge hard genoeg mept
                if (collision.relativeVelocity.x > 6f
                    || collision.relativeVelocity.y > 6f
                    || collision.relativeVelocity.z > 6f)
                {
                    var AI = collision.gameObject.GetComponentInParent<AIBehavior>();
                    if (AI != null)
                        AI.HandlePunchFromPlayer();
                }
                break;
            case "Gieter":
                //Gieter gevonden! pak de gieter!
                var gieterScript = collision.gameObject.GetComponent<VolleGieterbehavior>();
                if (gieterScript != null)
                    gieterScript.HouGieterVast(gameObject);
                break;
            default:
                // weetikveelwadeesis
                break;
        }
    }
}
