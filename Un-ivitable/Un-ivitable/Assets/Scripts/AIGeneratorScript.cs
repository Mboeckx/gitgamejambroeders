using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class AIGeneratorScript : MonoBehaviour
{
    public GameObject AITemplate;
    HeadStyleHolder HeadStyleHolderInstance;
    SpawnPointHolder SpawnPointHolderInstance;
    BodyStyleHolder BodyStyleHolderInstance;

    public float FastestSpawnTimeInFrames = 120;
    public float SlowestSpawnTimeInFrames = 400;

    private float framesUntilNextSpawn;
    public bool IsSpawnActive;

    // Start is called before the first frame update
    void Start()
    {
        HeadStyleHolderInstance = FindObjectOfType<HeadStyleHolder>();
        SpawnPointHolderInstance = FindObjectOfType<SpawnPointHolder>();
        BodyStyleHolderInstance = FindObjectOfType<BodyStyleHolder>();
        ResetSpawnTimer();
        IsSpawnActive = true;
    }

    private void ResetSpawnTimer()
    {
        framesUntilNextSpawn = Random.Range(FastestSpawnTimeInFrames, SlowestSpawnTimeInFrames);
    }

    // Update is called once per frame
    void Update()
    {
        if (IsSpawnActive)
        {
            if (framesUntilNextSpawn <= 0)
            {
                ResetSpawnTimer();
                SpawnCollega();
            }
        }
        
        //manuele spawn (DEBUG) kan ook als isSpawnActive false staat
        //if (Input.GetKeyDown(KeyCode.A))
        //{
        //    SpawnCollega();
        //}
    }

    private void FixedUpdate()
    {
        framesUntilNextSpawn--;
    }

    private void SpawnCollega()
    {
        //Spawn AI in one of the spawnpoints
        var newAIinstance = Instantiate(AITemplate, SpawnPointHolderInstance.SpawnPointCollection[Random.Range(0, SpawnPointHolderInstance.SpawnPointCollection.Count - 1)].position, new Quaternion(0, -90f, 0, 0));

        //choose random headstyle
        var chosenHedStyle = HeadStyleHolderInstance.HeadStyles[Random.Range(0, HeadStyleHolderInstance.HeadStyles.Length)];
        var hed = newAIinstance.transform.Find("Hoofd");
        hed.GetComponent<MeshFilter>().sharedMesh = chosenHedStyle.Meshes;
        hed.GetComponent<MeshRenderer>().sharedMaterial = chosenHedStyle.Materials;

        //choose random headstyle
        //var availableBodyStyles = BodyStyleHolderInstance.BodyStyles.Where(x => x.Gender != "M").ToList();
        var chosenBodyStyle = BodyStyleHolderInstance.BodyStyles[Random.Range(0, BodyStyleHolderInstance.BodyStyles.Length)];
        var body = newAIinstance.transform.Find("Lijf");
        body.GetComponent<MeshFilter>().sharedMesh = chosenBodyStyle.Meshes;
        body.GetComponent<MeshRenderer>().sharedMaterial = chosenBodyStyle.Materials;
        //body.GetComponent<MeshCollider>().sharedMesh = chosenBodyStyle.Meshes;
    }
}
