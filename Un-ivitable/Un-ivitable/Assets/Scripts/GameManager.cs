using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public float score = 0f;
    public static bool gameover;
    bool gameoveractionsTaken;

    public GameObject GameOverPopup;

    public static Mesh hoofdMesh;
    public static Mesh bodyMesh;
    public static Mesh handMesh;

    public static Material handMaterial;
    public static Material hoofdMaterial;
    public static Material bodyMaterial;

    public static bool WaterButtonPressed;
    public static bool FillButtonPressed;

    public AudioSource BackgroundMusicPlayer;
    public AudioClip GameOverMusic;

    private Text scoreText;

    public Text highScoreText;

    private GameObject BloemObject;

    // Start is called before the first frame update
    void Start()
    {
        WaterButtonPressed = false;
        FillButtonPressed = false;
        gameover = false;

        gameoveractionsTaken = false;

        var handstyleholder = GameObject.Find("HolderDeBolder").GetComponent<HandStyleHolder>();
        var headstyleholder = GameObject.Find("HolderDeBolder").GetComponent<HeadStyleHolder>();
        var bodystyleholder = GameObject.Find("HolderDeBolder").GetComponent<BodyStyleHolder>();
        var plantstyleholder = GameObject.Find("HolderDeBolder").GetComponent<PlantStyleHolder>();

        var Player = GameObject.Find("Ventje");

        Scene scene = SceneManager.GetActiveScene();
        if (scene.name == "SampleScene")
        {
            if (GlobalSettings.controlScheme == Assets.Scripts.ControlScheme.WASD)
            {
                GameObject.Find("Overlay/TouchButtons").SetActive(false);
            }

            Player.transform.Find("Hoofd").GetComponent<MeshFilter>().sharedMesh = headstyleholder.HeadStyles[GlobalSettings.currentHoofd].Meshes;
            Player.transform.Find("Hoofd").GetComponent<MeshRenderer>().sharedMaterial = headstyleholder.HeadStyles[GlobalSettings.currentHoofd].Materials;

            Player.transform.Find("Lijf").GetComponent<MeshFilter>().sharedMesh = bodystyleholder.BodyStyles[GlobalSettings.currentBody].Meshes;
            Player.transform.Find("Lijf").GetComponent<MeshCollider>().sharedMesh = bodystyleholder.BodyStyles[GlobalSettings.currentBody].Meshes;
            Player.transform.Find("Lijf").GetComponent<MeshRenderer>().sharedMaterial = bodystyleholder.BodyStyles[GlobalSettings.currentBody].Materials;

            Player.transform.Find("LHand").GetComponent<MeshFilter>().sharedMesh = handstyleholder.HandStyles[GlobalSettings.currentHand].Meshes;
            Player.transform.Find("LHand").GetComponent<MeshCollider>().sharedMesh = handstyleholder.HandStyles[GlobalSettings.currentHand].Meshes;
            Player.transform.Find("LHand").GetComponent<MeshRenderer>().sharedMaterial = handstyleholder.HandStyles[GlobalSettings.currentHand].Materials;

            Player.transform.Find("RHand").GetComponent<MeshFilter>().sharedMesh = handstyleholder.HandStyles[GlobalSettings.currentHand].Meshes;
            Player.transform.Find("RHand").GetComponent<MeshCollider>().sharedMesh = handstyleholder.HandStyles[GlobalSettings.currentHand].Meshes;
            Player.transform.Find("RHand").GetComponent<MeshRenderer>().sharedMaterial = handstyleholder.HandStyles[GlobalSettings.currentHand].Materials;

            BloemObject = GameObject.Find("Bloem/Bloem");
            int chosenPlant = Random.Range(0, plantstyleholder.PlantStyles.Length);
            BloemObject.GetComponent<MeshFilter>().sharedMesh = plantstyleholder.PlantStyles[chosenPlant].HappyMesh;
            //BloemObject.GetComponent<MeshCollider>().sharedMesh = plantstyleholder.PlantStyles[chosenPlant].HappyMesh;

            BloemObject.GetComponent<MeshRenderer>().sharedMaterial = plantstyleholder.PlantStyles[chosenPlant].HappyMaterial;
            BloemObject.GetComponent<PlantDorstBehavior>().OldMesh = plantstyleholder.PlantStyles[chosenPlant].HappyMesh;
            BloemObject.GetComponent<PlantDorstBehavior>().OldPalette = plantstyleholder.PlantStyles[chosenPlant].HappyMaterial;
            BloemObject.GetComponent<PlantDorstBehavior>().NewMesh = plantstyleholder.PlantStyles[chosenPlant].SadMesh;
            BloemObject.GetComponent<PlantDorstBehavior>().NewPalette = plantstyleholder.PlantStyles[chosenPlant].SadMaterial;

        }

        AudioSource bgmPlayer = UpdateAudioSourceVolumes();

        if (scene.name == "SampleScene")
        {
            scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
            var audioEffectsHolder = GameObject.Find("HolderDeBolder").GetComponent<AudioEffectsHolder>();
            var bgms = audioEffectsHolder.AudioEffects.Where(x => x.Tag == "bgm").ToList();

            bgmPlayer.clip = bgms[Random.Range(0, bgms.Count)].clip;
            bgmPlayer.loop = true;
            bgmPlayer.Play();
        }
    }

    private static AudioSource UpdateAudioSourceVolumes()
    {
        var audiosources = Resources.FindObjectsOfTypeAll<AudioSource>().ToList();
        foreach (var audiosource in audiosources)
        {
            audiosource.volume = GlobalSettings.SFXVolume;
        }

        var bgmPlayer = GameObject.Find("Main Camera").GetComponent<AudioSource>();

        bgmPlayer.volume = GlobalSettings.BGMVolume;
        return bgmPlayer;
    }

    // Update is called once per frame
    void Update()
    {
        if (highScoreText != null)
        {
            GlobalSettings.HighScore = PlayerPrefs.GetInt("HighScore");
            highScoreText.text = "High score: " + GlobalSettings.HighScore.ToString() + "s";
        }
        
        if (!gameover)
        {
            if (scoreText != null) {
                score += Time.deltaTime;
                scoreText.text = (int)score + "s";
            }
       
        }
        else
        {
            if (!gameoveractionsTaken)
            {
                BackgroundMusicPlayer.clip = GameOverMusic;
                BackgroundMusicPlayer.loop = false;
                BackgroundMusicPlayer.Play();

                gameoveractionsTaken = true;
                if(GlobalSettings.HighScore < (int)score)
                {
                    GlobalSettings.HighScore = (int)score;
                    PlayerPrefs.SetInt("HighScore", GlobalSettings.HighScore);
                    PlayerPrefs.Save();
                }

                GameObject.Find("ScoreLabel").GetComponent<Text>().text = "It was inevitable";
                GameOverPopup.SetActive(true);
                GameOverPopup.transform.Find("GameOverText").GetComponent<Text>().text = SpeechBubbles.GameOverInsults[Random.Range(0, SpeechBubbles.GameOverInsults.Count)];
            }
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            RestartGame();
        }
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.M) || Input.GetKeyDown(KeyCode.Backspace))
        {
            BackToMenu();
        }
        
    }

    public static void RestartGame()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }

    public static void BackToMenu()
    {
        SceneManager.LoadScene("ConanScene");
    }
}
