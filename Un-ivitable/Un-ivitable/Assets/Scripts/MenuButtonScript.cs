using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuButtonScript : MonoBehaviour
{
    public GameObject buttonStart;
    public GameObject buttonCreate;
    public GameObject buttonBack;
    public GameObject buttonHeadNext;
    public GameObject buttonBodyNext;
    public GameObject buttonHeadPrevious;
    public GameObject buttonBodyPrevious;
    public GameObject buttonHands;
    public GameObject buttonCredits;
    public GameObject buttonSettings;
    public GameObject buttonBackFromCredits;

    public GameObject gameByText;
    public GameObject highscoreText;
    public GameObject vickyText;
    public GameObject conanText;
    public GameObject metiorText;

    public GameObject SettingsPanel;
    public Slider BGMSlider;
    public Slider SFXSlider;
    public Button ControlSchemeButton;

    public AudioSource audioSource;

    List<string> controlSchemeOptions = new List<string>() { "Gyro (hold like box)", "Gyro (hold up)", "Virtual Joystick", "WASD / Arrows" };
    // Start is called before the first frame update
    void Start()
    {
        GlobalSettings.SFXVolume = PlayerPrefs.GetFloat("SFXVolume", 1f);
        GlobalSettings.BGMVolume = PlayerPrefs.GetFloat("BGMVolume", 1f);
        BGMSlider.value = GlobalSettings.BGMVolume;
        SFXSlider.value = GlobalSettings.SFXVolume;

        if(Application.platform == RuntimePlatform.Android)
        {
            controlSchemeOptions.Remove("WASD / Arrows");
        }

        audioSource.volume = GlobalSettings.BGMVolume;

        SettingsPanel.SetActive(false);
        string savedScheme = PlayerPrefs.GetString("ChosenControlScheme", "Gyro (hold like box)");

        int selectedControlScheme = controlSchemeOptions.IndexOf(savedScheme);
        ControlSchemeButton.GetComponentInChildren<Text>().text = controlSchemeOptions[selectedControlScheme];
        SaveAndApplyControlScheme(savedScheme);
    }

    private static void SaveAndApplyControlScheme(string savedScheme)
    {
        PlayerPrefs.SetString("ChosenControlScheme", savedScheme);
        PlayerPrefs.Save();
        if (savedScheme == "Gyro (hold like box)")
        {
            GlobalSettings.controlScheme = Assets.Scripts.ControlScheme.GyroTouch;
            GlobalSettings.gyroYOffset = 0f;
        }
        else if (savedScheme == "Gyro (hold up)")
        {
            GlobalSettings.controlScheme = Assets.Scripts.ControlScheme.GyroTouch;
            GlobalSettings.gyroYOffset = 0.75f;
        }
        else if (savedScheme == "Virtual Joystick")
        {
            GlobalSettings.controlScheme = Assets.Scripts.ControlScheme.VirtualStickTouch;
            GlobalSettings.gyroYOffset = 0f;
        }
        else if (savedScheme == "WASD / Arrows")
        {
            GlobalSettings.controlScheme = Assets.Scripts.ControlScheme.WASD;
            GlobalSettings.gyroYOffset = 0f;
        }
        else
        {
            //WASD default
            GlobalSettings.controlScheme = Assets.Scripts.ControlScheme.WASD;
            GlobalSettings.gyroYOffset = 0f;
        }
    }

    public void enableButton(GameObject onButton)
    {
        onButton.SetActive(true);
    }

    // function to disable
    public void disableButton(GameObject offButton)
    {
        offButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        if (SettingsPanel.activeSelf)
        {
            audioSource.volume = BGMSlider.value;
        }
    }

    public void StartGame()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void doeDeDramaZoomNaarHetCharacter(int i)
    {
        //also hide buttons
        //also show back button
        //also show outfit buttons
        FindObjectOfType<Camera>().GetComponent<Animator>().Play("Dramazoom");
        buttonStart.SetActive(false);
        buttonCreate.SetActive(false);
        buttonCredits.SetActive(false);
        buttonSettings.SetActive(false);

        buttonBack.SetActive(true);
        buttonHeadNext.SetActive(true);
        buttonBodyNext.SetActive(true);
        buttonHeadPrevious.SetActive(true);
        buttonBodyPrevious.SetActive(true);
        buttonHands.SetActive(true);

        var deCube = GameObject.Find("Ventje").transform.Find("Cube");
        var hoofd = GameObject.Find("Ventje").transform.Find("Hoofd");
        deCube.GetComponent<Rigidbody>().isKinematic = true;
        hoofd.GetComponent<Rigidbody>().isKinematic = true;
        hoofd.transform.position = new Vector3(-1.9f, -0.4f, -5.2f);

        hoofd.transform.rotation = new Quaternion(0, 0, 0, 0);
    }

    public void doeDeDramaZoomNaarCredits(int i)
    {
        //also hide buttons
        //also show back button
        //also show outfit buttons
        FindObjectOfType<Camera>().GetComponent<Animator>().Play("DramaZoomCredits");
        buttonStart.SetActive(false);
        buttonSettings.SetActive(false);
        buttonCreate.SetActive(false);
        buttonCredits.SetActive(false);
        highscoreText.SetActive(false);
        new WaitForSeconds(6);
        StartCoroutine(Text());
        


    }
    IEnumerator Text()  //  <-  its a standalone method
    {
       
        yield return new WaitForSeconds(1);
        vickyText.SetActive(true);
        metiorText.SetActive(true);
        conanText.SetActive(true);
        gameByText.SetActive(true);
        buttonBackFromCredits.SetActive(true);
    }
    public void doeDeDramaZoomWegVanCredits(int i)
    {
        //also hide buttons
        //also show back button
        //also show outfit buttons
        FindObjectOfType<Camera>().GetComponent<Animator>().Play("doeDeDramaZoomWegVanCredits");

        buttonSettings.SetActive(true);
        buttonStart.SetActive(true);
        buttonCreate.SetActive(true);
        buttonCredits.SetActive(true);
        highscoreText.SetActive(true);

        vickyText.SetActive(false);
        metiorText.SetActive(false);
        conanText.SetActive(false);
        gameByText.SetActive(false);

        buttonBackFromCredits.SetActive(false);
    }
    public void doeDeDramaZoomOurEnReset(int i)
    {
        //also hide buttons
        //also show back button
        //also show outfit buttons
        FindObjectOfType<Camera>().GetComponent<Animator>().Play("Dramazoomout");
        buttonStart.SetActive(true);
        buttonCreate.SetActive(true);
        buttonSettings.SetActive(true);
        buttonBack.SetActive(false);
        buttonCredits.SetActive(true);
        buttonHeadNext.SetActive(false);
        buttonBodyNext.SetActive(false);
        buttonHeadPrevious.SetActive(false);
        buttonBodyPrevious.SetActive(false);
        buttonHands.SetActive(false);

        PlayerPrefs.SetInt("currentHoofd", GlobalSettings.currentHoofd);
        PlayerPrefs.SetInt("currentBody", GlobalSettings.currentBody);
        PlayerPrefs.SetInt("currentHand", GlobalSettings.currentHand);

        PlayerPrefs.Save();
    }

    public void volgendHoofd()
    {

        var HeadStyle = GameObject.Find("HolderDeBolder").GetComponent<HeadStyleHolder>();
        
        int nextHeadStyle = 0;
        if (GlobalSettings.currentHoofd + 1 < HeadStyle.HeadStyles.Length)
        {
            nextHeadStyle = GlobalSettings.currentHoofd + 1;
        }
        else
        {
            nextHeadStyle = 0;
        }
        GlobalSettings.currentHoofd = nextHeadStyle;



        GameObject.Find("Ventje").transform.Find("Hoofd").GetComponent<MeshRenderer>().sharedMaterial = HeadStyle.HeadStyles[nextHeadStyle].Materials;
            GameObject.Find("Ventje").transform.Find("Hoofd").GetComponent<MeshFilter>().sharedMesh = HeadStyle.HeadStyles[nextHeadStyle].Meshes;
    }

    public void vorigHoofd()
    {

        var HeadStyle = GameObject.Find("HolderDeBolder").GetComponent<HeadStyleHolder>();

        int nextHeadStyle = 0;
        if (GlobalSettings.currentHoofd - 1 >= 0)
        {
            nextHeadStyle = GlobalSettings.currentHoofd - 1;
        }
        else
        {
            nextHeadStyle = HeadStyle.HeadStyles.Length-1;
        }
        GlobalSettings.currentHoofd = nextHeadStyle;



        GameObject.Find("Ventje").transform.Find("Hoofd").GetComponent<MeshRenderer>().sharedMaterial = HeadStyle.HeadStyles[nextHeadStyle].Materials;
        GameObject.Find("Ventje").transform.Find("Hoofd").GetComponent<MeshFilter>().sharedMesh = HeadStyle.HeadStyles[nextHeadStyle].Meshes;
    }

    public void vorigBody()
    {

        var BodyStyle = GameObject.Find("HolderDeBolder").GetComponent<BodyStyleHolder>();

        int nextBodyStyle = 0;
        if (GlobalSettings.currentBody - 1 >= 0)
        {
            nextBodyStyle = GlobalSettings.currentBody - 1;
        }
        else
        {
            nextBodyStyle = BodyStyle.BodyStyles.Length - 1;
        }
        GlobalSettings.currentBody = nextBodyStyle;



        GameObject.Find("Ventje").transform.Find("Lijf").GetComponent<MeshRenderer>().sharedMaterial = BodyStyle.BodyStyles[nextBodyStyle].Materials;
        GameObject.Find("Ventje").transform.Find("Lijf").GetComponent<MeshFilter>().sharedMesh = BodyStyle.BodyStyles[nextBodyStyle].Meshes;
    }

    public void volgendBody()
    {
        var BodyStyle = GameObject.Find("HolderDeBolder").GetComponent<BodyStyleHolder>();

        int nextBodyStyle = 0;
        if (GlobalSettings.currentBody + 1 < BodyStyle.BodyStyles.Length)
        {
            nextBodyStyle = GlobalSettings.currentBody + 1;
        }
        else
        {
            nextBodyStyle = 0;
        }
        GlobalSettings.currentBody = nextBodyStyle;



        GameObject.Find("Ventje").transform.Find("Lijf").GetComponent<MeshRenderer>().sharedMaterial = BodyStyle.BodyStyles[nextBodyStyle].Materials;
        GameObject.Find("Ventje").transform.Find("Lijf").GetComponent<MeshFilter>().sharedMesh = BodyStyle.BodyStyles[nextBodyStyle].Meshes;
    }

    public void volgendHand()
    {

        var HandStyle = GameObject.Find("HolderDeBolder").GetComponent<HandStyleHolder>();

        int nextHandStyle = 0;
        if (GlobalSettings.currentHand + 1 < HandStyle.HandStyles.Length)
        {
            nextHandStyle = GlobalSettings.currentHand + 1;
        }
        else
        {
            nextHandStyle = 0;
        }
        GlobalSettings.currentHand = nextHandStyle;



        GameObject.Find("Ventje").transform.Find("RHand").GetComponent<MeshRenderer>().sharedMaterial = HandStyle.HandStyles[nextHandStyle].Materials;
        GameObject.Find("Ventje").transform.Find("RHand").GetComponent<MeshFilter>().sharedMesh = HandStyle.HandStyles[nextHandStyle].Meshes;
        GameObject.Find("Ventje").transform.Find("LHand").GetComponent<MeshRenderer>().sharedMaterial = HandStyle.HandStyles[nextHandStyle].Materials;
        GameObject.Find("Ventje").transform.Find("LHand").GetComponent<MeshFilter>().sharedMesh = HandStyle.HandStyles[nextHandStyle].Meshes;
    }

    public void ToggleControlScheme()
    {
        string schemeClicked = ControlSchemeButton.GetComponentInChildren<Text>().text;
        int clickedSchemeId = controlSchemeOptions.IndexOf(schemeClicked);

        string newScheme = "";
        if (clickedSchemeId < controlSchemeOptions.Count - 1)
        {
            newScheme = controlSchemeOptions[clickedSchemeId + 1];
        }
        else
        {
            newScheme = controlSchemeOptions[0];
        }

        ControlSchemeButton.GetComponentInChildren<Text>().text = newScheme;
        SaveAndApplyControlScheme(newScheme);
    }

    public void ToggleSettingsVisible()
    {
        if (SettingsPanel.activeSelf)
        {
            //save
            GlobalSettings.BGMVolume = BGMSlider.value;
            GlobalSettings.SFXVolume = SFXSlider.value;
            PlayerPrefs.SetFloat("SFXVolume", GlobalSettings.SFXVolume);
            PlayerPrefs.SetFloat("BGMVolume", GlobalSettings.BGMVolume);
            PlayerPrefs.Save();

            //show other menu buttons again
            buttonStart.SetActive(true);
            buttonCredits.SetActive(true);
            buttonSettings.SetActive(true);
            buttonCreate.SetActive(true);
        }
        else
        {
            //hide other menu buttons, show settings
            buttonStart.SetActive(false);
            buttonCredits.SetActive(false);
            buttonSettings.SetActive(false);
            buttonCreate.SetActive(false);
        }

        SettingsPanel.SetActive(!SettingsPanel.activeSelf);
    }
}


