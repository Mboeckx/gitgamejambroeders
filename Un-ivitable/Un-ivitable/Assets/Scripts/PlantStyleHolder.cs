using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable] //om te kunnen editen in de inspector
public class PlantStyle
{
    public Material HappyMaterial;
    public Mesh HappyMesh;
    public Material SadMaterial;
    public Mesh SadMesh;
    public string Type;
    public string Tag;
}

public class PlantStyleHolder : MonoBehaviour
{

    //Singleton
    public static PlantStyleHolder Instance;

    public PlantStyle[] PlantStyles;

    void Awake()
    {
        Instance = this;
    }
}