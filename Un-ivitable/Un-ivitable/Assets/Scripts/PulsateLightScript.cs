using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulsateLightScript : MonoBehaviour
{
    public Light light;
    bool goUp;

    public float pulsateSpeed = 0.003f;
    public bool Stop;
    float standardintensity;

    // Start is called before the first frame update
    void Start()
    {
        goUp = true;
        light = GetComponent<Light>();
        standardintensity = light.intensity;
    }

    // Update is called once per frame
    void Update()
    {
        if (!Stop)
        {
            if(light.intensity <= 0.3f) {
                goUp = true;
            }else if(light.intensity >= standardintensity)
            {
                goUp = false;
            }


            if (goUp)
            {
                light.intensity += pulsateSpeed;
            }
            else
            {
                light.intensity -= pulsateSpeed;
            }
        }
        else
        {
            light.intensity = 0f;
        }
    }

    public void SetColor(Color color)
    {
        light.color = color;
    }
}
