using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class floorCollScript : MonoBehaviour
{
    public AudioSource muziek;
    public Canvas deUI;
    public GameObject hetVentje;

    // Start is called before the first frame update
    void Start()
    {
        muziek = GetComponent<AudioSource>();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player") {
            deUI.enabled = true;
        }
        if (!muziek.isPlaying)
        {
            hetVentje.SetActive(true);
            loadSavedVentje();
            muziek.volume = GlobalSettings.BGMVolume;
            muziek.Play();
            
        }
    }

    public void loadSavedVentje()
    {
        GlobalSettings.currentHoofd = PlayerPrefs.GetInt("currentHoofd");
        GlobalSettings.currentBody = PlayerPrefs.GetInt("currentBody");
        GlobalSettings.currentHand = PlayerPrefs.GetInt("currentHand");

        var HeadStyle = GameObject.Find("HolderDeBolder").GetComponent<HeadStyleHolder>();
        GameObject.Find("Ventje").transform.Find("Hoofd").GetComponent<MeshRenderer>().sharedMaterial = HeadStyle.HeadStyles[GlobalSettings.currentHoofd].Materials;
        GameObject.Find("Ventje").transform.Find("Hoofd").GetComponent<MeshFilter>().sharedMesh = HeadStyle.HeadStyles[GlobalSettings.currentHoofd].Meshes;

        var HandStyle = GameObject.Find("HolderDeBolder").GetComponent<HandStyleHolder>();
        GameObject.Find("Ventje").transform.Find("RHand").GetComponent<MeshRenderer>().sharedMaterial = HandStyle.HandStyles[GlobalSettings.currentHand].Materials;
        GameObject.Find("Ventje").transform.Find("RHand").GetComponent<MeshFilter>().sharedMesh = HandStyle.HandStyles[GlobalSettings.currentHand].Meshes;
        GameObject.Find("Ventje").transform.Find("LHand").GetComponent<MeshRenderer>().sharedMaterial = HandStyle.HandStyles[GlobalSettings.currentHand].Materials;
        GameObject.Find("Ventje").transform.Find("LHand").GetComponent<MeshFilter>().sharedMesh = HandStyle.HandStyles[GlobalSettings.currentHand].Meshes;

        var BodyStyle = GameObject.Find("HolderDeBolder").GetComponent<BodyStyleHolder>();
        GameObject.Find("Ventje").transform.Find("Lijf").GetComponent<MeshRenderer>().sharedMaterial = BodyStyle.BodyStyles[GlobalSettings.currentBody].Materials;
        GameObject.Find("Ventje").transform.Find("Lijf").GetComponent<MeshFilter>().sharedMesh = BodyStyle.BodyStyles[GlobalSettings.currentBody].Meshes;
    }
}
