using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class AIBehavior : MonoBehaviour
{
    public int Health;
    NavMeshAgent agent;
    public float TijdTotGoestingOmBloemAanTeRaken;

    private float wachtNogEfkes;

    List<PraatpuntConfig> _praatpunten;

    List<Vector3> historyOfPaths;

    private bool gameoverhandled;

    public GameObject Explosion;

    // Start is called before the first frame update
    void Start()
    {
        gameoverhandled = false;
        Health = 100;
        SetRandomTijdTotGoestingOmBloemAanTeRaken();
        _praatpunten = GameObject.Find("PraatPunten").GetComponentsInChildren<PraatpuntConfig>().ToList();

        historyOfPaths = new List<Vector3>();

        wachtNogEfkes = 0;

        agent = GetComponentInChildren<NavMeshAgent>();
    }


    public void SetRandomTijdTotGoestingOmBloemAanTeRaken(float minTijd = 0f, float maxTijd = 30f)
    {
        TijdTotGoestingOmBloemAanTeRaken = Random.Range(minTijd, maxTijd);
    }


    // Update is called once per frame
    void Update()
    {
        Vector3? newDestination = null;
        if(Health> 0)
        {
            if (agent.pathStatus == NavMeshPathStatus.PathComplete || agent.pathStatus == NavMeshPathStatus.PathInvalid)
            {
                if (TijdTotGoestingOmBloemAanTeRaken > 0f)
                {

                    //nog geen goesting om de bloem aan te raken
                    //ga naar praatplek

                    //zoek of er al volk is op praatpunt
                    List<PraatpuntConfig> praatpuntenwaariemandis = _praatpunten.Where(x => x.HoeveelVolkKomtNaarHier > 0).ToList();
                    //maar niet iedereen is graag sociaal
                    bool wilNaarAnderVolk = false;

                    //als er volk is ergens, kijk dan of we sociaal genoeg zijn om erheen te gaan
                    if (praatpuntenwaariemandis.Any())
                    {
                        wilNaarAnderVolk = Random.Range(0, 10) > 3;
                    }

                    //misschien heeft em ook gewoon helemaal geen goesting om te bewegen, maar enkel als em al ergens is.
                    if (historyOfPaths.Any())
                    {
                        if (Random.Range(0, 10) > 5)
                        {
                            wachtNogEfkes = 1f;
                        }
                    }

                    if (wachtNogEfkes <= 0)
                    {
                        //niet aan het wachten
                        if (wilNaarAnderVolk)
                        {
                            int index = Random.Range(0, praatpuntenwaariemandis.Count);
                            newDestination = praatpuntenwaariemandis[index].Location;
                            praatpuntenwaariemandis[index].HoeveelVolkKomtNaarHier++;
                        }
                        else
                        {
                            int index = Random.Range(0, _praatpunten.Count);
                            newDestination = _praatpunten[index].Location;
                            _praatpunten[index].HoeveelVolkKomtNaarHier++;
                        }
                    }
                }
                else
                {
                    //het is tijd om de bloem aan te raken
                    newDestination = GameObject.Find("Bloem").transform.position;
                }

                if (newDestination != null)
                {
                    agent.SetDestination(newDestination.Value);
                    historyOfPaths.Add(newDestination.Value);
                }
            }
        }

        if (!gameoverhandled)
        {
            if (GameManager.gameover)
            {
                gameoverhandled = true;
                GetComponentInChildren<SpeechBubbleBehavior>().DecideNextText(true);
            }
        }
    }

    public void FixedUpdate()
    {
        if (wachtNogEfkes > 0)
        {
            wachtNogEfkes -= (1f / 60f);
        }
        TijdTotGoestingOmBloemAanTeRaken -= (1f / 60f);
    }

    public void HandlePunchFromPlayer()
    {
        if(Health > 0)
        {
            GetComponentInChildren<NavMeshAgent>().isStopped = true;
            Destroy(transform.Find("Cube").gameObject);

            Health = 0;

            GetComponentInChildren<SpeechBubbleBehavior>().DecideNextText(true);
            Destroy(gameObject, 10f);
        }
        
    }

    private void OnDestroy()
    {
        Explode();
    }

    public void Explode()
    {
        var rotation = Quaternion.FromToRotation(Vector3.up, new Vector3(0, 0, 0));
        var transform = this.transform.Find("Hoofd").GetComponent<Transform>().position;
        Instantiate(Explosion, transform, rotation);
    }
}
