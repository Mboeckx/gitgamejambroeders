﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable] //om te kunnen editen in de inspector
public class AudioEffects
{
    public AudioClip clip;
    public string Tag;
}

public class AudioEffectsHolder : MonoBehaviour
{

    //Singleton
    public static AudioEffectsHolder Instance;

    public AudioEffects[] AudioEffects;

    void Awake()
    {
        Instance = this;
    }

    public void PlayAudioClip(AudioClip clip)
    {
        var source = GetComponent<AudioSource>();
        source.clip = clip;
        source.Play();
    }
}